<?php
/**
 * @author Dainius Pivoras (maintenance)
 * @copyright Siuntų centras, UAB
 * @license Commercial
 */

/**
 * Class AllParcelsConfigSourceData
 */
class AllParcelsConfigSourceData
{
    /**
     * Options getter
     *
     * @return array
     */
    public static function toOptionArray()
    {
        $result = array();
        foreach (self::carriersArray() as $key => $value) {
            $result[] = array('value' => $key, 'label' => $value);
        }

        return $result;
    }

    public static function carriersArray()
    {
        return array(
            'POST24_LT' => 'Omniva LT',
            'POST24_LV' => 'Omniva LV',
            'POST24_EE' => 'Omniva EE',
            'LP_EXPRESS' => 'LP Express',
            'VENIPAK' => 'Venipak',
            'DPD_LT' => 'DPD LT',
            'DPD_LV' => 'DPD LV',
            'DPD_EE' => 'DPD EE',
            'UPS' => 'UPS LT',
            'UPS_LV' => 'UPS LV',
            'UPS_EE' => 'UPS EE',
            'TNT_LT' => 'TNT LT',
            'TNT_LV' => 'TNT LV',
            'TNT_EE' => 'TNT EE',
            'DHL' => 'DHL',
            'LT_POST' => 'LT paštas',
            'ITELLA' => 'Itella',
        );
    }
    /**
     * Get array for delivery options
     *
     * @return array
     */
    public static function deliveryOptionsArray()
    {
        return array(
            0 => 'Hands',
            1 => 'Parcel terminal',
            2 => 'Post office',
            3 => 'Pickup point',
        );
    }

    /**
     * Box sizes array
     *
     * @return array
     */
    public static function boxSizesArray()
    {
        return array(
            'LP_EXPRESS' => array(
                'S' => 'S (8x35x61 cm)',
                'M' => 'M (17.5x35x61 cm)',
                'L' => 'L (36.5x35x61 cm)',
                'XL' => 'XL (74.5x35x61 cm)',
            ),
            'POST24' => array(
                'S' => 'S (9x38x64 cm)',
                'M' => 'M (19x38x64 cm)',
                'L' => 'L (39x38x64 cm)',
            )
        );
    }
}
