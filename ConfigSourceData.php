<?php

class AllParcels_ConfigSourceData
{
    /**
     * Options getter
     *
     * @return array
     */
    static public function toOptionArray()
    {
        $result = [];
        foreach (self::carriersArray() as $key => $value) {
            $result[] = ['value' => $key, 'label' => $value];
        }

        return $result;
    }

    static public function carriersArray()
    {
        return [
            'POST24_LT' => 'Omniva LT',
            'POST24_LV' => 'Omniva LV',
            'POST24_EE' => 'Omniva EE',
            'LP_EXPRESS' => 'LP Express',
            'VENIPAK' => 'Venipak',
            'DPD_LT' => 'DPD LT',
            'DPD_LV' => 'DPD LV',
            'DPD_EE' => 'DPD EE',
            'UPS' => 'UPS LT',
            'UPS_LV' => 'UPS LV',
            'UPS_EE' => 'UPS EE',
            'TNT_LT' => 'TNT LT',
            'TNT_LV' => 'TNT LV',
            'TNT_EE' => 'TNT EE',
            'DHL' => 'DHL',
            'LT_POST' => 'LT paštas',
            'ITELLA' => 'Itella',
        ];
    }
    /**
     * Get array for delivery options
     *
     * @return array
     */
    static public function deliveryOptionsArray()
    {
        return [
            0 => 'Hands',
            1 => 'Parcel terminal',
            2 => 'Post office',
            3 => 'Pickup point',
        ];
    }

    /**
     * Box sizes array
     *
     * @return array
     */
    static public function boxSizesArray()
    {
        return [
            'LP_EXPRESS' => [
                'S' => 'S (8x35x61 cm)',
                'M' => 'M (17.5x35x61 cm)',
                'L' => 'L (36.5x35x61 cm)',
                'XL' => 'XL (74.5x35x61 cm)',
            ],
            'POST24' => [
                'S' => 'S (9x38x64 cm)',
                'M' => 'M (19x38x64 cm)',
                'L' => 'L (39x38x64 cm)',
            ]
        ];
    }
}