# ALLPARCELS PRESTASHOP MODULE #

This is prestashop module created to communicate with http://toast.allparcels.com system.

### How to use it? ###

* Download module from https://bitbucket.org/barzda/allparcels-ps/downloads or clone our repository.
* Install module to your shop.
* Configure it.
* Use it.

### Requirements ###

* Prestashop 1.5 version or higher
* PHP 5.3 or more