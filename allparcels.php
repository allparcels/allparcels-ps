<?php
/**
 * @author Dainius Pivoras (maintenance)
 * @copyright Siuntų centras, UAB
 * @license Commercial
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(_PS_MODULE_DIR_ . 'allparcels/models/Array2XML.php');
require_once(_PS_MODULE_DIR_ . 'allparcels/models/CreateFile.php');
require_once(_PS_MODULE_DIR_ . 'allparcels/ConfigSourceData.php');
require_once(_PS_MODULE_DIR_ . 'allparcels/controllers/admin/TerminalsController.php');

class AllParcels extends Module
{
    const CONST_PREFIX = 'ALLPARCELS_';
    public static $_moduleName = 'allparcels';
    public $id_carrier;
    private $express_available = array('TNT', 'DHL', 'UPS');
    private $cod_methods = array('cashondelivery', 'codfee');

    public function __construct()
    {
        $this->name = 'allparcels';
        $this->tab = 'shipping_logistics';
        $this->version = '1.4.9';
        $this->author = 'allparcels.com';
        $this->need_instance = 0;
        //$this->ps_versions_compliancy = ['min' => '1.5', 'max' => _PS_VERSION_];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('allparcels.com');
        $this->description = $this->l('allparcels.com shipping module');

        $this->confirmUninstall = $this->l('Are you sure want to uninstall?');
        $this->_errors = array();
        if (!Configuration::get('ALLPARCELS')) {
            $this->warning = $this->l('No name provided');
        }
    }

    /**
     * @return bool
     */
    public function install()
    {
        if (!parent::install() || !$this->registerHook('actionAdminControllerSetMedia')
            || !$this->registerHook('displayCarrierList') || !$this->registerHook('actionValidateOrder')
            || !$this->registerHook('displayAdminOrder') || !$this->registerHook('displayOrderDetail')
            || !$this->registerHook('displayShoppingCartFooter')
        ) {
            return false;
        }

        if (!$this->createTerminalsTable() || !$this->createIndex() || !$this->updateOrderTable() || !$this->updateCartTable()) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    private function createTerminalsTable()
    {
        $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'terminals`(
            `id` int(11) unsigned NOT NULL auto_increment,
            `identifier` varchar(255) NOT NULL,
            `name` varchar(255) NOT NULL,
            `address` varchar(255) NOT NULL,
            `city` varchar(255) NULL,
            `post_code` varchar(255) NULL,
            `country` varchar(2) NULL,
            `comment` text NULL,
            `type` int(11) unsigned NOT NULL,
            `carrier_code` varchar(255) NOT NULL,
            `is_active` int(1) NULL,

            `created_time` datetime NULL,
            `update_time` datetime NULL,
             PRIMARY KEY (`id`)
	    ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        return Db::getInstance()->execute($sql);
    }

    /**
     * @return bool
     */
    private function createIndex()
    {
        $sql = 'ALTER TABLE `' . _DB_PREFIX_ . 'terminals` ADD INDEX terminal_identifier (identifier);';

        return Db::getInstance()->execute($sql);
    }

    /**
     * @return bool
     */
    private function updateOrderTable()
    {
        $sql = 'ALTER TABLE `' . _DB_PREFIX_ . 'orders` ADD terminal_id varchar(255) NULL;';
        $sql .= 'ALTER TABLE `' . _DB_PREFIX_ . 'orders` ADD carrier_slug varchar(255) NULL;';
        $sql .= 'ALTER TABLE `' . _DB_PREFIX_ . 'orders` ADD box_size varchar(255) NULL;';

        return Db::getInstance()->execute($sql);
    }

    /**
     * @return bool
     */
    private function updateCartTable()
    {
        $sql = 'ALTER TABLE `' . _DB_PREFIX_ . 'cart` ADD terminal_id varchar(255) NULL;';

        return Db::getInstance()->execute($sql);
    }

    /**
     * @return bool
     */
    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }
        if (!$this->freeTables()) {
            return false;
        }

        $settings = array(
            self::CONST_PREFIX . 'SETTINGS',
        );

        foreach ($settings as $setting) {
            Configuration::deleteByName($setting);
        }

        return true;
    }

    /**
     * @return bool
     */
    private function freeTables()
    {
        $sql = 'ALTER TABLE `' . _DB_PREFIX_ . 'terminals` DROP INDEX terminal_identifier;';
        $sql .= 'ALTER TABLE `' . _DB_PREFIX_ . 'orders` DROP COLUMN terminal_id, DROP COLUMN carrier_slug, DROP COLUMN box_size;';
        $sql .= 'ALTER TABLE `' . _DB_PREFIX_ . 'cart` DROP COLUMN terminal_id;';
        $sql .= 'DROP TABLE `' . _DB_PREFIX_ . 'terminals`;';

        return Db::getInstance()->execute($sql);
    }

    /**
     * @param $parent
     * @param $class_name
     * @param $name
     * @return mixed
     */
    public function installTab($parent, $class_name, $name)
    {
        $tab = new Tab();
        $tab->id_parent = (int)Tab::getIdFromClassName($parent);
        $tab->name = array();

        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $name;
        }

        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->active = 1;

        return $tab->add();
    }

    /**
     * For adding CSS,JS scripts
     */
    public function hookActionAdminControllerSetMedia()
    {
        if (Tools::getIsset('configure')) {
            if (Tools::getValue('configure') == self::$_moduleName) {
                $this->context->controller->addCSS($this->_path . 'views/css/' . self::$_moduleName . '.css', 'all');
                $this->context->controller->addJS($this->_path . 'views/js/' . self::$_moduleName . '.js');
            }
        }
    }

    /**
     *
     */
    public function hookActionValidateOrder($params)
    {
        $sql = 'SELECT terminal_id FROM ' . _DB_PREFIX_ . 'cart WHERE id_cart = ' . $params['cart']->id;
        $terminalId = Db::getInstance()->executeS($sql);
        if ($terminalId) {
            $terminalId = $terminalId[0]['terminal_id'];
            $sql = 'SELECT carrier_code FROM ' . _DB_PREFIX_ . 'terminals WHERE identifier = ' . $terminalId;
            $carrierCode = Db::getInstance()->executeS($sql);
            Db::getInstance()->update('orders', array(
                'terminal_id' => $terminalId,
                'box_size' => Configuration::get(self::CONST_PREFIX . 'DEFAULT_BOXSIZE'),
                'carrier_slug' => $carrierCode[0]['carrier_code'],
            ), 'id_order = ' . $params['order']->id);
        } else {
            $carrier = $this->getData($params['order']->id_carrier, 'default_courier');
            if ($this->getData($params['order']->id_carrier, 'drop_off')) {
                Db::getInstance()->update('orders', array(
                    'carrier_slug' => $carrier,
                    'box_size' => Configuration::get(self::CONST_PREFIX . 'DEFAULT_BOXSIZE'),
                ), 'id_order = ' . $params['order']->id);
            } else {
                Db::getInstance()->update('orders', array(
                    'carrier_slug' => $carrier,
                ), 'id_order = ' . $params['order']->id);
            }
        }
    }

    /**
     * @param $carrier
     * @param null $field
     * @return array|int
     */
    public function getData($carrier, $field = null)
    {
        $data = (unserialize(Configuration::get(self::CONST_PREFIX . 'SETTINGS')));

        if (!in_array($carrier, $data['shipping_methods'])) {
            return array();
        }

        $line = array_flip($data['shipping_methods']);
        $data = $this->flipArrayList($data);

        if ($field == 'express' && !$this->checkExpress($carrier)) {
            return 0;
        }

        if ($field) {
            if (isset($data[$line[$carrier]][$field])) {
                return $data[$line[$carrier]][$field];
            }
            return null;
        }

        return $data[$line[$carrier]];
    }

    /**
     * Flip DualDimensional Array
     *
     * @param array
     * @return  array
     */
    public function flipArrayList($list)
    {
        $array = array();
        if ($list && count($list)) {
            $options = array_keys($list);
            $values_count = array_keys($list[$options[0]]);
            foreach ($values_count as $row) {
                foreach ($options as $option) {
                    if (isset($list[$option][$row])) {
                        $array[$row][$option] = $list[$option][$row];
                    }
                }
            }
        }

        return $array;
    }

    /**
     * @param $carrierId
     * @return bool
     */
    private function checkExpress($carrierId)
    {
        $carrier = new Carrier($carrierId);

        return in_array($carrier->name, $this->express_available);
    }

    /**
     *
     */
    public function hookDisplayOrderDetail($params)
    {
        $sql = 'SELECT terminal_id FROM ' . _DB_PREFIX_ . 'orders WHERE id_order = ' . $params['order']->id;
        $terminalId = Db::getInstance()->executeS($sql);
        $terminalId = $terminalId[0]['terminal_id'];
        if (!$terminalId) {
            return '';
        }
        $address = new Address($params['order']->id_address_delivery);
        $countryCode = country::getIsoById($address->id_country);
        $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'terminals WHERE identifier = ' . $terminalId . ' AND country = "' . $countryCode . '"';
        $terminal = Db::getInstance()->executeS($sql);

        if ($terminal) {
            $terminal = $terminal[0];
            echo '<div class="info-order box">
	            <p><strong class="dark">' . $this->l('Terminal / Post office') . ': </strong>' . $terminal['name'] . ' - ' . $terminal['address']
                . (($terminal['post_code']) ? (', ' . $terminal['country'] . '-' . $terminal['post_code']) : '') . '</p>
			</div>';
        }

        return '';
    }

    /**
     *
     */
    public function hookDisplayAdminOrder($params)
    {
        $order = new Order($params['id_order']);
        $carrierSettings = $this->getData($order->id_carrier);
        $sql = 'SELECT carrier_slug FROM ' . _DB_PREFIX_ . 'orders WHERE id_order = ' . $params['id_order'];
        $carrierSlug = Db::getInstance()->executeS($sql);
        if ($carrierSlug) {
            $carrierSlug = $carrierSlug[0]['carrier_slug'];
        }
        $sql = 'SELECT terminal_id FROM ' . _DB_PREFIX_ . 'orders WHERE id_order = ' . $params['id_order'];
        $terminalId = Db::getInstance()->executeS($sql);
        $terminalId = $terminalId[0]['terminal_id'];
        $html = '';
        if ($terminalId) {
            $address = new Address($params['cart']->id_address_delivery);
            $countryCode = country::getIsoById($address->id_country);
            $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'terminals WHERE identifier = ' . $terminalId . ' AND country = "' . $countryCode . '"';
            $terminal = Db::getInstance()->executeS($sql);
            if ($terminal) {
                $terminal = $terminal[0];
                $html = '<div class="panel col-lg-6" style="margin-right: 10px;">
					<div class="panel-heading">
						<i class="icon-truck"></i>
						' . $this->l('Terminal / Post office') . '
					</div>
						<div>' . $terminal['identifier'] . ' ' . $terminal['name'] . ' - ' . $terminal['address'] . ', ' . $terminal['city']
                    . (($terminal['post_code']) ? (', ' . $terminal['country'] . '-' . $terminal['post_code']) : '') . '</div>
                </div>';
            }
        }
        if ($terminalId || $carrierSettings['drop_off']) {
            $sizes = array();
            foreach (AllParcelsConfigSourceData::boxSizesArray() as $slug => $array) {
                if (explode('_', $slug)[0] == explode('_', $carrierSlug)[0]) {
                    $sizes = $array;
                }
            }
            if ($sizes) {
                $sql = 'SELECT box_size FROM ' . _DB_PREFIX_ . 'orders WHERE id_order = ' . $params['id_order'];
                $boxSize = Db::getInstance()->executeS($sql);
                if ($boxSize) {
                    $boxSize = $boxSize[0]['box_size'];
                }
                $options = '<option/>';
                foreach ($sizes as $key => $size) {
                    $options .= '<option value="' . $key . '" ' . (($key == $boxSize) ? 'selected="selected"' : '') . '>' . $size . '</option>';
                }

                $html .= '<div class="panel col-lg-6" style="margin-right: 10px;">
					<div class="panel-heading">
						<i class="icon-truck"></i>
						' . $this->l('Box size') . '
					</div>
						<div>
                            <select name="boxsize" id="boxsize"  style="width:20%; display:inline;vertical-align: bottom;">' . $options . '</select>
                            <button class="btn btn-primary" type="submit" id="submitChangeBoxSize">
                                ' . $this->l('Change') . '
                            </button>
						</div>
                    </div>';
                $html .= '<script>';
                $html .= '$("#submitChangeBoxSize").click(function(){
                    var boxsize = $("select#boxsize").val();
                    if (typeof boxsize === "undefined") {
                        return false;
                    } else {
                        var data = "boxsize=" + boxsize + "&order=' . $params['id_order'] . '";
                    }
                    $.ajax({
                        type: "POST",
                        url: "/modules/allparcels/changeBoxsize.php",
                        data: data,
                        dataType: "json"
                    });
                });';
                $html .= '</script>';
            }
        }
        if ($carrierSettings['shipping_delivery_option'] !== 0 || count($carrierSettings['shipping_identifier']) < 2) {
            return $html;
        }
        $select = '<select id="courier_change" style="width:20%; display:inline;vertical-align: bottom;">';
        foreach ($carrierSettings['shipping_identifier'] as $carrier) {
            $select .= '<option value="' . $carrier . '"' . (($carrierSlug == $carrier) ? 'selected' : '') . '>' . AllParcelsConfigSourceData::carriersArray()[$carrier] . '</option>';
        }
        $select .= '</select>';
        $html .= '<div class="panel col-lg-6">
					<div class="panel-heading">
						<i class="icon-truck"></i>
						' . $this->l('Change courier') . '
					</div>
					<div>' . $select . '
					<button class="btn btn-primary" type="submit" id="submitChangeCourier">
                        ' . $this->l('Change') . '
					</button>
					</div>
                </div>';
        $html .= '<script>';
        $html .= '$("#submitChangeCourier").click(function(){
            var carrier = $("select#courier_change").val();
            if (typeof carrier === "undefined") {
                return false;
            } else {
                var data = "carrier=" + carrier + "&order=' . $params['id_order'] . '";
            }
            $.ajax({
                type: "POST",
                url: "/modules/allparcels/changeCarrier.php",
                data: data,
                dataType: "json"
            });
        });';
        $html .= '</script>';

        return $html;
    }

    public function hookdisplayShoppingCartFooter()
    {
        $this->context->controller->addJS($this->_path . 'views/js/terminalSelect.js');
    }

    /**
     * @param $params
     * @return bool|string
     */
    public function hookdisplayCarrierList($params)
    {
        $this->context->controller->addJS($this->_path . 'views/js/terminalSelect.js');

        $carrier = $params['cart']->id_carrier;
        $sql = 'SELECT terminal_id FROM ' . _DB_PREFIX_ . 'cart WHERE id_cart = ' . $params['cart']->id;
        $terminalId = Db::getInstance()->executeS($sql);
        if ($terminalId) {
            $terminalId = $terminalId[0]['terminal_id'];
        }
        $carriersConfig = $this->flipArrayList(unserialize(Configuration::get(self::CONST_PREFIX . 'SETTINGS')));
        $carriers = array();
        foreach ($carriersConfig as $carrierConfig) {
            if (isset($carrierConfig['shipping_delivery_option']) && $carrierConfig['shipping_delivery_option'] > 0) {
                $sql = 'SELECT * FROM ' . _DB_PREFIX_ . 'terminals WHERE carrier_code IN ("' . implode('","', $carrierConfig['shipping_identifier']) .
                    '") AND type = "' . $carrierConfig['shipping_delivery_option'] . '" AND is_active = 1 AND country = "' . country::getIsoById($params['address']->id_country) . '" ORDER BY city ASC';
                $objects = Db::getInstance()->executeS($sql);
                $terminalsArray = array();
                foreach ($objects as $terminal) {
                    $terminalsArray[$terminal['city']][] = $terminal;
                }
                if ($terminalsArray) {
                    $html = '<select id="terminals" name="terminals" required="required"><option value="">' . $this->l('Please select') . '</option>';
                    foreach ($terminalsArray as $city => $terminals) {
                        $html .= '<optgroup label="' . $city . '">';
                        foreach ($terminals as $terminal) {
                            $html .= '<option ' . (($terminal['identifier'] == $terminalId) ? 'selected' : '') . ' value="' . $terminal['identifier'] . '">' . $terminal['name'] . ' - ' . $terminal['address']
                                . (($terminal['post_code']) ? (', ' . $terminal['country'] . '-' . $terminal['post_code']) : '') . '</option>';
                        }
                        $html .= '</optgroup>';
                    }
                    $html .= '</select>';
                    $carriers[$carrierConfig['shipping_methods']] = $html;
                }
            }
        }

        $html = '<table class="resume table table-bordered">
                    <tbody>
                        <tr>';
        if (isset($carriers[$carrier])) {
            $html .= '<td class="terminals_list">';
            $html .= $carriers[$carrier];
        } else {
            $html .= '<td class="terminals_list" style="display: none;">';
        }
        $html .= '</td>
                </tr>
            </tbody>
        </table>';

        $script = '<script type="text/javascript">var carriers = ' . json_encode($carriers) . ';</script>';

        return $html . $script;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        if (Tools::isSubmit('btnSubmit')) {
            $this->_postProcess();
        }
        if (Tools::isSubmit('updateTerminals')) {
            $this->updateTerminals();
        }
        $configs = $this->flipArrayList(unserialize(Configuration::get(self::CONST_PREFIX . 'SETTINGS')));
        $settings = array();
        foreach ($configs as $config) {
            $settings[$config['shipping_methods']] = $config;
        }
        $this->context->smarty->assign(
            array(
                'moduleName' => $this->name,
                'displayName' => $this->displayName,
                'action' => Tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']),
                'method' => 'POST',
                'api_token' => Configuration::get(self::CONST_PREFIX . 'API_TOKEN'),
                'boxSizes' => AllParcelsConfigSourceData::boxSizesArray(),
                'defaultBoxSize' => Configuration::get(self::CONST_PREFIX . 'DEFAULT_BOXSIZE'),
                'comment' => Configuration::get(self::CONST_PREFIX . 'COMMENT'),
                'all_carriers' => $this->getCarriersList(),
                'settings' => $settings,
                'delivery_options' => AllParcelsConfigSourceData::deliveryOptionsArray(), //TODO get from toaster
                'source_data' => AllParcelsConfigSourceData::toOptionArray(), //TODO get from toaster
            )
        );

        return $this->display(__FILE__, 'views/templates/admin/configure.tpl');
    }

    /**
     * @param array $skip
     */
    private function _postProcess($skip = array('tab', 'btnSubmit', 'updateTerminals'))
    {
        $data = $_POST;
        foreach ($data as $name => $value) {
            if (in_array($name, $skip)) {
                continue;
            }
            if (is_array($value)) {
                $value = serialize(Tools::getValue($name));
            } else {
                $value = Tools::getValue($name);
            }
            Configuration::updateValue(self::CONST_PREFIX . Tools::strtoupper($name), $value);
        }
    }

    /**
     * @return bool
     */
    private function updateTerminals()
    {
        $terminalsController = new TerminalsController();

        return $terminalsController->updateTerminals();
    }

    /**
     * @param null $id_lang
     * @return mixed
     */
    private function getCarriersList($id_lang = null)
    {
        if (empty($id_lang)) {
            $id_lang = $this->context->cookie->id_lang;
        }

        return Carrier::getCarriers($id_lang, false, false, false, null, 5);
    }

    /**
     * @param $orders
     * @return bool
     */
    public function updateTrackingNumbers($orders)
    {
        if (!isset($orders)) {
            return false;
        }

        $url = 'http://toast.allparcels.com/api/tracking_numbers.json';
        $token = Configuration::get('ALLPARCELS_API_TOKEN');
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "shipments=" . json_encode($orders));
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0';
        $headers[] = 'token: ' . $token;
        $headers[] = 'X-Requested-With: XMLHttpRequest';

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($curl);

        curl_close($curl);

        foreach (json_decode($result, true) as $orderId => $number) {
            Db::getInstance()->update('orders', array(
                'shipping_number' => $number
            ), 'id_order = ' . $orderId);
            Db::getInstance()->update('order_carrier', array(
                'tracking_number' => $number
            ), 'id_order = ' . $orderId);
            $order = new Order($orderId);
            Hook::exec('actionAdminOrdersTrackingNumberUpdate', array('order' => $order));
        }

        return true;
    }

    /**
     * @param $orders
     * @return bool
     */
    public function createXML($orders)
    {
        if (!isset($orders)) {
            return false;
        }

        $shipments = array(
            '@attributes' => array(
                'version' => 'prestashop_' . $this->version
            ),
            'shipment' => array()
        );
        foreach ($orders as $orderId) {
            $order = new Order($orderId);
            $address = new Address((int)$order->id_address_delivery);
            $phone_mobile = $this->validatePhone($address->phone_mobile);
            $phone_home = $this->validatePhone($address->phone);
            if (Tools::strlen($phone_mobile) >= 8) {
                $phone = $phone_mobile;
            } else {
                $phone = $phone_home;
            }
            $customer = new Customer((int)$order->id_customer);
            $cart = new Cart((int)$order->id_cart);
            $sql = 'SELECT terminal_id, carrier_slug, box_size FROM ' . _DB_PREFIX_ . 'orders WHERE id_order = ' . $orderId;
            $data = Db::getInstance()->executeS($sql);
            $shipment = array(
                'reference' => $orderId,
                'weight' => (float)number_format($cart->getTotalWeight(), 3, '.', ''),
                'remark' => '',
                'additional_information' => '',
                'number_of_parcels' => 1,
                'courier_identifier' => ($data[0]['carrier_slug']) ? $data[0]['carrier_slug'] : $this->getData($order->id_carrier, 'default_courier'),
                'receiver' => array(
                    'name' => $address->firstname . ' ' . $address->lastname,
                    'street' => $address->address1,
                    'postal_code' => $address->postcode,
                    'city' => $address->city,
                    'phone' => $phone,
                    'email' => $customer->email,
                    'parcel_terminal_identifier' => ($data[0]['terminal_id']) ? $data[0]['terminal_id'] : '',
                    'country_code' => country::getIsoById($address->id_country),
                ),
                'services' => array(
                    'cash_on_delivery' => $this->getCod($orderId),
                    'express_delivery' => $this->getData($order->id_carrier, 'express'),
                    'saturday_delivery' => $this->getData($order->id_carrier, 'saturday'),
                    'document_return' => $this->getData($order->id_carrier, 'bbx'),
                    'drop_off' => $this->getData($order->id_carrier, 'drop_off'),
                    'inform_sender_email' => $this->getData($order->id_carrier, 'inform_sender_email'),
                    'inform_sender_sms' => $this->getData($order->id_carrier, 'inform_sender_sms'),
                    'inform_receiver_email' => $this->getData($order->id_carrier, 'inform_receiver_email'),
                    'inform_receiver_sms' => $this->getData($order->id_carrier, 'inform_receiver_sms')
                )
            );
            if ($data[0]['box_size']) {
                $shipment['box_size'] = $data[0]['box_size'];
            }
            foreach ($shipment['services'] as $key => $value) {
                if (!$value) {
                    unset($shipment['services'][$key]);
                }
            }
            $shipments['shipment'][] = $shipment;
        }
        $xml = Array2XML::createXML('shipments', $shipments);

        return $this->download('shipments.xml', $xml->saveXML(), 'xml');
    }

    /**
     * @param $phone
     * @return mixed|string
     */
    private function validatePhone($phone)
    {
        $phone = trim($phone);
        if (Tools::strlen($phone) >= 8) {
            return '8'.Tools::substr($phone, -8);
        }

        return $phone;
    }

    /**
     * @param $orderId
     * @return array
     */
    public function getCod($orderId)
    {
        $order = new Order($orderId);
        $currency = new Currency($order->id_currency);
        $cod = array();
        if (in_array($order->module, $this->cod_methods)) {
            $cod = array(
                'value' => number_format($order->total_paid, 2, '.', ''),
                'reference' => $order->reference,
                'currency' => $currency->iso_code
            );
        }

        return $cod;
    }

    /**
     * @param $filename
     * @param $content
     * @param string $fileType
     * @return bool
     */
    private function download($filename, $content, $fileType)
    {
        $cretedownload = new CreateFile($filename, $fileType, true);

        return $cretedownload->render($content);
    }
}
