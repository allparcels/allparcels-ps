<?php

require_once(dirname(__FILE__) . '../../../config/config.inc.php');
require_once(dirname(__FILE__) . '../../../init.php');

die(Tools::jsonEncode(Db::getInstance()->update('orders', [
    'box_size' => (Tools::getValue('boxsize')) ? Tools::getValue('boxsize') : NULL
], 'id_order = ' . Tools::getValue('order'))));