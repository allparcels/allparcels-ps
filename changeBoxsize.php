<?php
/**
 * @author Dainius Pivoras (maintenance)
 * @copyright Siuntų centras, UAB
 * @license Commercial
 */

require_once(dirname(__FILE__) . '../../../config/config.inc.php');
require_once(dirname(__FILE__) . '../../../init.php');

/**
 * Mini controller
 */
die(Tools::jsonEncode(Db::getInstance()->update('orders', array(
    'box_size' => (Tools::getValue('boxsize')) ? Tools::getValue('boxsize') : null
), 'id_order = ' . Tools::getValue('order'))));
