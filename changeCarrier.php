<?php

require_once(dirname(__FILE__) . '../../../config/config.inc.php');
require_once(dirname(__FILE__) . '../../../init.php');

die(Tools::jsonEncode(Db::getInstance()->update('orders', [
    'carrier_slug' => (Tools::getValue('carrier')) ? Tools::getValue('carrier') : NULL
], 'id_order = ' . Tools::getValue('order'))));