<?php
/**
 * @author Dainius Pivoras (maintenance)
 * @copyright Siuntų centras, UAB
 * @license Commercial
 */

/**
 * Mini controller
 */
require_once(dirname(__FILE__) . '../../../config/config.inc.php');
require_once(dirname(__FILE__) . '../../../init.php');

die(
    Tools::jsonEncode(
        Db::getInstance()->update(
            'orders',
            array('carrier_slug' => (Tools::getValue('carrier')) ? Tools::getValue('carrier') : null),
            'id_order = ' . Tools::getValue('order')
        )
    )
);
