<?php

/**
 * Created by PhpStorm.
 * User: deividas
 * Date: 15.9.24
 * Time: 15.51
 */
class TerminalsController
{
    /**
     * Update terminals
     *
     * @return bool
     */
    public function updateTerminals()
    {
        $terminals = json_decode($this->getTerminalsList(), true);
        if (!isset($terminals['terminals'])) {
            return false;
        }
        $identifiers = [];
        foreach ($terminals['terminals'] as $terminal) {
            $identifiers[$terminal['identifier']] = $terminal['identifier'];
        }
        $sql = 'SELECT id, identifier FROM ' . _DB_PREFIX_ . 'terminals WHERE identifier IN (' . implode(',', $identifiers) . ')';
        $existing = Db::getInstance()->executeS($sql);
        $existingTerminals = [];
        foreach ($existing as $term) {
            $existingTerminals[$term['identifier']] = $term['id'];
        }
        foreach ($terminals['terminals'] as $terminal) {
            if (isset($existingTerminals[$terminal['identifier']])) {
                Db::getInstance()->update('terminals', [
                    'name' => $terminal['name'],
                    'address' => $terminal['address'],
                    'city' => $terminal['city'],
                    'post_code' => $terminal['postCode'],
                    'country' => $terminal['countryCode'],
                    'comment' => $terminal['comment'],
                    'type' => $terminal['type'],
                    'is_active' => $terminal['isActive'],
                    'carrier_code' => $terminal['courierIdentifier'],
                    'update_time' => date('Y-m-d H:i:s')
                ], 'id = ' . $existingTerminals[$terminal['identifier']]);
            } else {
                Db::getInstance()->insert('terminals', [
                    'identifier' => $terminal['identifier'],
                    'name' => $terminal['name'],
                    'address' => $terminal['address'],
                    'city' => $terminal['city'],
                    'post_code' => $terminal['postCode'],
                    'country' => $terminal['countryCode'],
                    'comment' => $terminal['comment'],
                    'type' => $terminal['type'],
                    'is_active' => $terminal['isActive'],
                    'carrier_code' => $terminal['courierIdentifier'],
                    'created_time' => date('Y-m-d H:i:s'),
                    'update_time' => date('Y-m-d H:i:s')
                ]);
            }
        }

        return true;
    }

    /**
     * Get terminals list from toaster
     *
     * @return mixed
     */
    private function getTerminalsList()
    {
        $url = 'http://toast.allparcels.com/api/parcel_terminals.json?showAll=1';
        $token = Configuration::get('ALLPARCELS_API_TOKEN');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        $headers = [];
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0';
        $headers[] = 'token: ' . $token;
        $headers[] = 'X-Requested-With: XMLHttpRequest';

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }
}