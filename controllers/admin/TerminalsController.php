<?php
/**
 * @author Dainius Pivoras (maintenance)
 * @copyright Siuntų centras, UAB
 * @license Commercial
 */

class TerminalsController
{
    /**
     * Update terminals
     *
     * @return bool
     */
    public function updateTerminals()
    {
        $terminals = json_decode($this->getTerminalsList(), true);
        if (!isset($terminals['terminals'])) {
            return false;
        }
        $identifiers = array();
        foreach ($terminals['terminals'] as $terminal) {
            $identifiers[$terminal['identifier']] = pSQL($terminal['identifier']);
        }
        $sql = "SELECT id, identifier FROM "._DB_PREFIX_."terminals WHERE identifier IN ('".implode("','", $identifiers)."')";

        $existing = Db::getInstance()->executeS($sql);
        $existingTerminals = array();
        foreach ($existing as $term) {
            $existingTerminals[$term['identifier']] = $term['id'];
        }
        foreach ($terminals['terminals'] as $terminal) {
            if (isset($existingTerminals[$terminal['identifier']])) {
                Db::getInstance()->update('terminals', array(
                    'name' => pSQL($terminal['name']),
                    'address' => pSQL($terminal['address']),
                    'city' => pSQL($terminal['city']),
                    'post_code' => pSQL($terminal['postCode']),
                    'country' => pSQL($terminal['countryCode']),
                    'comment' => pSQL($terminal['comment']),
                    'type' => pSQL($terminal['type']),
                    'is_active' => pSQL($terminal['isActive']),
                    'carrier_code' => pSQL($terminal['courierIdentifier']),
                    'update_time' => date('Y-m-d H:i:s')
                ), 'id = ' . $existingTerminals[$terminal['identifier']]);
            } else {
                Db::getInstance()->insert('terminals', array(
                    'identifier' => pSQL($terminal['identifier']),
                    'name' => pSQL($terminal['name']),
                    'address' => pSQL($terminal['address']),
                    'city' => pSQL($terminal['city']),
                    'post_code' => pSQL($terminal['postCode']),
                    'country' => pSQL($terminal['countryCode']),
                    'comment' => pSQL($terminal['comment']),
                    'type' => pSQL($terminal['type']),
                    'is_active' => pSQL($terminal['isActive']),
                    'carrier_code' => pSQL($terminal['courierIdentifier']),
                    'created_time' => date('Y-m-d H:i:s'),
                    'update_time' => date('Y-m-d H:i:s')
                ));
            }
        }

        return true;
    }

    /**
     * Get terminals list from toaster
     *
     * @return mixed
     */
    private function getTerminalsList()
    {
        $url = 'http://toast.allparcels.com/api/parcel_terminals.json?showAll=1';
        $token = Configuration::get('ALLPARCELS_API_TOKEN');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array();
        $headers[] = 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0';
        $headers[] = 'token: ' . $token;
        $headers[] = 'X-Requested-With: XMLHttpRequest';

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }
}
