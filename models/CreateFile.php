<?php
/**
 * @author Dainius Pivoras (maintenance)
 * @copyright Siuntų centras, UAB
 * @license Commercial
 */

/**
 * Class CreateFile
 */
class CreateFile
{
    //filename to download the file under
    private $filename = '';
    //the mimetype of the file
    private $mimetype = '';
    //length of the file
    private $filesize = 0;
    //disallow multi-threaded downloading
    private $forceSingle = false;
    //in multi-threaded downloading, the offset to start at
    private $range = 0;

    /**
     * @param string $filename
     * @param string $mimetype
     * @param bool|false $forceSingle
     */
    public function __construct($filename = '', $mimetype = 'application/octet-stream', $forceSingle = false)
    {
        //import members
        $this->forceSingle = $forceSingle;
        $this->filename = $filename;
        $this->mimetype = $mimetype;
        //if safe mode is enabled, raise a warning
        if (ini_get('safe_mode')) {
            trigger_error('<b>Downloader:</b> Will not be able to handle large files while safe mode is enabled.' . E_USER_WARNING);
        }
    }

    /**
     * @param string $filename
     * @return string
     * @throws Exception
     */
    public function downloadFile($filename = '')
    {
        //assert the file is valid
        if (!is_file($filename)) {
            throw new Exception('Downloader: Could not find file \'' . $filename . '\'');
        }
        //make sure it's read-able
        if (!is_readable($filename)) {
            throw new Exception('Downloader: File was unreadable \'' . $filename . '\'');
        }
        //set script execution time to 0 so the script
        //won't time out.
        set_time_limit(0);
        //get the size of the file
        $this->filesize = filesize($filename);
        //set up the main headers
        //find out the number of bytes to write in this iteration
        $block_size = $this->prepareHeaders($this->filesize);
        /* output the file itself */
        $chunksize = 1 * (1024 * 1024);
        $bytes_send = 0;
        if ($file = fopen($filename, 'r')) {
            if (isset($_SERVER['HTTP_RANGE']) && !$this->forceSingle) {
                fseek($file, $this->range);
            }
            //write the data out to the browser
            while (!feof($file) && !connection_aborted() && $bytes_send < $block_size) {
                $buffer = fread($file, $chunksize);
                echo $buffer;
                flush();
                $bytes_send += Tools::strlen($buffer);
            }
            fclose($file);
        } else {
            throw new Exception('Downloader: Could not open file \'' . $filename . '\'');
        }

        return "";
    }

    /**
     * @param int $size
     * @return int
     */
    private function prepareHeaders($size = 0)
    {
        if (ob_get_contents()) {
            print_r('Some data has already been output');
            die();
        }
        // required for IE, otherwise Content-Disposition may be ignored
        if (ini_get('zlib.output_compression')) {
            ini_set('zlib.output_compression', 'Off');
        }
        header('Content-Type: ' . $this->mimetype, false);
        header('Content-Disposition: attachment; filename="' . $this->filename . '"');

        return $size;
    }

    /**
     * @param $content
     * @return string
     */
    public function render($content)
    {
        set_time_limit(0);
        $this->prepareHeaders(Tools::strlen($content));
        echo $content;

        die;
    }
}
