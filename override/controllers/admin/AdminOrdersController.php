<?php
/**
 * @author Dainius Pivoras (maintenance)
 * @copyright Siuntų centras, UAB
 * @license Commercial
 */

/**
 * Class AdminOrdersController
 */
class AdminOrdersController extends AdminOrdersControllerCore
{
    public function __construct()
    {
        parent::__construct();
        $this->bulk_actions['GenerateAllparcelsXML'] = array(
            'text' => Translate::getModuleTranslation('allparcels', 'Export as XML', 'configure')
        );
        $this->bulk_actions['GetAllparcelsTrackCodes'] = array(
            'text' => Translate::getModuleTranslation('allparcels', 'Update tracking number', 'configure')
        );
    }

    public function processBulkGenerateAllparcelsXML()
    {
        $order_ids = Tools::getValue('orderBox');
        $allparcels = Module::getInstanceByName('allparcels');
        $allparcels->createXML($order_ids);
    }

    public function processBulkGetAllparcelsTrackCodes()
    {
        $order_ids = Tools::getValue('orderBox');
        $allparcels = Module::getInstanceByName('allparcels');
        $allparcels->updateTrackingNumbers($order_ids);
    }
}
