<?php
/**
 * @author Dainius Pivoras (maintenance)
 * @copyright Siuntų centras, UAB
 * @license Commercial
 */

require_once(dirname(__FILE__) . '../../../config/config.inc.php');
require_once(dirname(__FILE__) . '../../../init.php');

/**
 * Mini controller
 */
if (Tools::getValue('terminal')) {
    if (!isset($context)) {
        $context = Context::getContext();
    }
    Db::getInstance()->update(
        'cart',
        array(
            'terminal_id' => (Tools::getValue('terminal')) ? Tools::getValue('terminal') : null
        ),
        'id_cart = ' . (isset($context->cookie->id_cart) ? (int)$context->cookie->id_cart : 0)
    );

    $result = Tools::jsonEncode(
        array(
            'terminal_id' => Tools::getValue('terminal'),
            'id_cart' => (isset($context->cookie->id_cart) ? (int)$context->cookie->id_cart : 0)
        )
    );
    die($result);
} else {
    die('Terminal value not found');
}
