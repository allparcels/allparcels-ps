/**
 * @author Dainius Pivoras (maintenance)
 * @copyright Siuntų centras, UAB
 * @license Commercial
 */

jQuery(document).ready(function () {
    allparcels.event = {
        addListener: function (template, heading) {
            var template = jQuery(template).html();
            jQuery(heading).append(template);
        },
        remove: function (dom) {
            jQuery(dom).closest('tr').remove();
        }
    };
    $('.courier').change(function () {
        if ($(this).is(':checked')) {
            var opt = document.createElement('option');
            opt.value = $(this).val();
            opt.innerHTML = $(this).next('span').text();
            $(this).parent().next('td').find('.default_courier').append(opt);
        } else {
            $(this).parent().next('td').find('.default_courier option[value="' + $(this).val() + '"]').remove();
        }
    });
    var settings = JSON.parse(apsettings);
    $('.courier').each(function (key, value) {
        if ($(value).is(':checked')) {
            var select = $(value).parent().next('td').find('.default_courier');
            var opt = document.createElement('option');
            opt.value = $(this).val();
            opt.innerHTML = $(this).next('span').text();
            if (settings[select.closest('tr').attr('id')]['default_courier'] == opt.value) {
                opt.setAttribute('selected', 'selected');
            }
            select.append(opt);
        } else {
            $(value).parent().next('td').find('.default_courier option[value="' + $(this).val() + '"]').remove();
        }
    })
});