/**
 * @author Dainius Pivoras (maintenance)
 * @copyright Siuntų centras, UAB
 * @license Commercial
 */

jQuery(document).ready(function () {
    var getTerminal = function() {
        var terminal = $("select#terminals").val();
        if (typeof terminal === "undefined" || terminal === '') {
            return 0;
        } else {
            return terminal;
        }
    };

    var submitTerminal = function() {
        $.ajax({
            type: "POST",
            url: baseDir + "modules/allparcels/terminals.php",
            data: 'terminal=' + getTerminal(),
            dataType: "json"
        });
    };

    $(document).on('change', 'input.delivery_option_radio', function() {
        $("td.terminals_list").empty();
        // carriers comes from template
        $("td.terminals_list").html(carriers[$(this).val().replace(",", "")]);
    });

    $(document).on('submit', 'form#form', submitTerminal);
    $(document).on('click', '.place_order', submitTerminal);

    // Hardcode fixup for advanced checkout module
    var toggleButtons = function() {
        submitTerminal();
        if(carriers[$('input.delivery_option_radio:checked').val().replace(',', '')] && getTerminal() === 0) {
            $('#place_order_default').hide();
            $('#place_order_allparcels').show();
        } else {
            $('#place_order_default').show();
            $('#place_order_allparcels').hide();
       }
    };

    if($('.place_order')) {
        $('.place_order').attr('id', 'place_order_default').parent()
            .append($('.place_order').clone().attr('id', 'place_order_allparcels'));

        $(document).on('click', '#place_order_allparcels', function() {
            alert('Terminal is not selected');
        });
        $(document).on('change', 'input.delivery_option_radio', toggleButtons);
        $(document).on('change', 'select#terminals', toggleButtons);
        toggleButtons();
    }
});
