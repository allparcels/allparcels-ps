{**
 * @author Dainius Pivoras (maintenance)
 * @copyright Siuntų centras, UAB
 * @license Commercial
 *}
<div id="allparcels">
    <img src="../modules/{$moduleName|escape:'htmlall':'UTF-8'}/logo.png" class="allparcels_logo_img">

    <h2 class="allparcels_inline">{$displayName|escape:'htmlall':'UTF-8'}</h2>

    <form action="{$action|escape:'quotes':'UTF-8'}" method="{$method|escape:'htmlall':'UTF-8'}">
        <fieldset>
            <legend><img src="../img/admin/contact.gif" alt=""/> {l s='Configuration' mod='allparcels'}</legend>
            <div class="margin-form">
                <label for="api_token">{l s='Token' mod='allparcels'} :</label>
                <input type="text" style="width:400px;" name="api_token" value="{$api_token|escape:'htmlall':'UTF-8'}"/>
            </div>
            <div class="margin-form">
                <label for="default_boxsize">{l s='Default box size' mod='allparcels'} :</label>
                <select style="width:400px;" name="default_boxsize">
                    {foreach from=$boxSizes key=carrier item=sizes}
                        <optgroup label="{$carrier|escape:'htmlall':'UTF-8'}">
                            {foreach from=$sizes key=size item=dimensions}
                                <option value="{$size|escape:'htmlall':'UTF-8'}" {if $defaultBoxSize eq $size} selected="selected" {/if}>
                                    {$dimensions|escape:'htmlall':'UTF-8'}
                                </option>
                            {/foreach}
                        </optgroup>
                    {/foreach}
                </select>

                <p>
                    <b>{l s='Please select one of package sizes. Notice that if you choose "M" size for LP Express courier, for other couriers will be used the same "M" size.' mod='allparcels'}</b>
                </p>
            </div>
            <label for="service_username">{l s='Services' mod='allparcels'} :</label>

            <div class="grid" id="grid_formFieldId ">
                <table cellpadding="0" cellspacing="0" class="border">
                    <thead>
                    <tr class="headings" id="heading">
                        <th>{l s='Shipping method' mod='allparcels'}</th>
                        <th>{l s='Delivery option' mod='allparcels'}</th>
                        <th>{l s='Courier' mod='allparcels'}</th>
                        <th>{l s='Default courier' mod='allparcels'}</th>
                        <th>{l s='Express' mod='allparcels'}</th>
                        <th>{l s='Saturday' mod='allparcels'}</th>
                        <th>{l s='Document return' mod='allparcels'}</th>
                        <th>{l s='Parcel put' mod='allparcels'}</th>
                        <th>{l s='Inform sender about sent shipment via email' mod='allparcels'}</th>
                        <th>{l s='Inform sender about sent shipment via sms' mod='allparcels'}</th>
                        <th>{l s='Inform receiver about waiting shipment via email' mod='allparcels'}</th>
                        <th>{l s='Inform receiver about waiting shipment via sms' mod='allparcels'}</th>
                    </tr>
                    </thead>
                    <tbody id="allparcels_body">
                    {if $all_carriers}
                        {foreach from=$all_carriers key=id item=carrier name=carrier}
                            {assign var="lineId" value=$carrier.id_carrier}
                            <tr class="headings" id="{$lineId|escape:'htmlall':'UTF-8'}">
                                <td class="select_sc">
                                    <select name="settings[shipping_methods][]" class="shipping_methods">
                                        <option value="{$carrier.id_carrier|escape:'htmlall':'UTF-8'}"
                                                selected="selected">{$carrier.name|escape:'htmlall':'UTF-8'}</option>
                                    </select>
                                </td>
                                <td nowrap="nowrap" class="select_do">
                                    <select name="settings[shipping_delivery_option][]"
                                            class="shipping_delivery_option">
                                        {foreach from=$delivery_options key=keyId item=value}
                                            <option value="{$keyId|escape:'htmlall':'UTF-8'}" {if isset($settings.$lineId.shipping_delivery_option) and $settings.$lineId.shipping_delivery_option eq $keyId} selected="selected" {/if}>{l s=$value mod='allparcels'}</option>
                                        {/foreach}
                                    </select>
                                </td>
                                <td nowrap="nowrap" class="select_c">
                                    {foreach from=$source_data key=id item=carrier}
                                        <input
                                                name="settings[shipping_identifier][{$smarty.foreach.carrier.index|escape:'htmlall':'UTF-8'}][]" {if isset($settings.$lineId.shipping_identifier) and $carrier.value|in_array:$settings.$lineId.shipping_identifier } checked {/if}
                                                class="courier" type="checkbox" value="{$carrier.value|escape:'htmlall':'UTF-8'}"/>
                                        <span>{$carrier.label|escape:'htmlall':'UTF-8'}</span>
                                        <br/>
                                    {/foreach}
                                </td>
                                <td class="select_dc">
                                    <select name="settings[default_courier][{$smarty.foreach.carrier.index|escape:'htmlall':'UTF-8'}]" class="default_courier">
                                    </select>
                                </td>
                                <td class="select_s">
                                    <select name="settings[express][]">
                                        <option value="0" {if isset($settings.$lineId) and $settings.$lineId.express eq "0"} selected="selected" {/if}>
                                            {l s='No' mod='allparcels'}
                                        </option>
                                        <option value="1" {if isset($settings.$lineId) and $settings.$lineId.express eq "1"} selected="selected" {/if}>
                                            {l s='Yes' mod='allparcels'}
                                        </option>
                                    </select>
                                </td>
                                <td class="select_s">
                                    <select name="settings[saturday][]">
                                        <option value="0"{if isset($settings.$lineId) and $settings.$lineId.saturday eq "0"} selected="selected" {/if}>
                                            {l s='No' mod='allparcels'}
                                        </option>
                                        <option value="1"{if isset($settings.$lineId) and $settings.$lineId.saturday eq "1"} selected="selected" {/if}>
                                            {l s='Yes' mod='allparcels'}
                                        </option>
                                    </select>
                                </td>
                                <td class="select_s">
                                    <select name="settings[bbx][]">
                                        <option value="0"{if isset($settings.$lineId) and $settings.$lineId.bbx eq "0"} selected="selected" {/if}>
                                            {l s='No' mod='allparcels'}
                                        </option>
                                        <option value="1"{if isset($settings.$lineId) and $settings.$lineId.bbx eq "1"} selected="selected" {/if}>
                                            {l s='Yes' mod='allparcels'}
                                        </option>
                                    </select>
                                </td>
                                <td nowrap="nowrap" class="select_s">
                                    <select name="settings[drop_off][]">
                                        <option value="0"{if isset($settings.$lineId) and $settings.$lineId.drop_off eq "0"} selected="selected" {/if}>
                                            {l s='No' mod='allparcels'}
                                        </option>
                                        <option value="1"{if isset($settings.$lineId) and $settings.$lineId.drop_off eq "1"} selected="selected" {/if}>
                                            {l s='Yes' mod='allparcels'}
                                        </option>
                                    </select>
                                </td>
                                <td class="select_s">
                                    <select name="settings[inform_sender_email][]">
                                        <option value="0"{if isset($settings.$lineId) and $settings.$lineId.inform_sender_email eq "0"} selected="selected" {/if}>
                                            {l s='No' mod='allparcels'}
                                        </option>
                                        <option value="1"{if isset($settings.$lineId) and $settings.$lineId.inform_sender_email eq "1"} selected="selected" {/if}>
                                            {l s='Yes' mod='allparcels'}
                                        </option>
                                    </select>
                                </td>
                                <td class="select_s">
                                    <select name="settings[inform_sender_sms][]">
                                        <option value="0"{if isset($settings.$lineId) and $settings.$lineId.inform_sender_sms eq "0"} selected="selected" {/if}>
                                            {l s='No' mod='allparcels'}
                                        </option>
                                        <option value="1"{if isset($settings.$lineId) and $settings.$lineId.inform_sender_sms eq "1"} selected="selected" {/if}>
                                            {l s='Yes' mod='allparcels'}
                                        </option>
                                    </select>
                                </td>
                                <td class="select_s">
                                    <select name="settings[inform_receiver_email][]">
                                        <option value="0"{if isset($settings.$lineId) and $settings.$lineId.inform_receiver_email eq "0"} selected="selected" {/if}>
                                            {l s='No' mod='allparcels'}
                                        </option>
                                        <option value="1"{if isset($settings.$lineId) and $settings.$lineId.inform_receiver_email eq "1"} selected="selected" {/if}>
                                            {l s='Yes' mod='allparcels'}
                                        </option>
                                    </select>
                                </td>
                                <td class="select_s">
                                    <select name="settings[inform_receiver_sms][]">
                                        <option value="0"{if isset($settings.$lineId) and $settings.$lineId.inform_receiver_sms eq "0"} selected="selected" {/if}>
                                            {l s='No' mod='allparcels'}
                                        </option>
                                        <option value="1"{if isset($settings.$lineId) and $settings.$lineId.inform_receiver_sms eq "1"} selected="selected" {/if}>
                                            {l s='Yes' mod='allparcels'}
                                        </option>
                                    </select>
                                </td>
                            </tr>
                        {/foreach}
                    {/if}
                    </tbody>
                </table>
            </div>
            <div class="margin-form">
                <input class="button" name="btnSubmit" value="{l s='Save settings' mod='allparcels'}" type="submit"/>
            </div>
            <div class="margin-form">
                <input class="button" name="updateTerminals"
                       value="{l s='Update Delivery Points' mod='allparcels'}"
                       type="submit"/>
            </div>
        </fieldset>
    </form>
</div>
<script type="text/javascript">
    var apsettings = '{$settings|@json_encode|escape:'javascript':'UTF-8'}';
</script>